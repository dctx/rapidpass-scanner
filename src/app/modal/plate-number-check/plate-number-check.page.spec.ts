import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PlateNumberCheckPage } from './plate-number-check.page';

describe('PlateNumberCheckPage', () => {
  let component: PlateNumberCheckPage;
  let fixture: ComponentFixture<PlateNumberCheckPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlateNumberCheckPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PlateNumberCheckPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
